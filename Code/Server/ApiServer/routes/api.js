var express = require('express')
const router = express.Router()
const jwt = require('jsonwebtoken')
const moment = require('moment')
const mailer = require('nodemailer')

const md5 = require('md5')
var randomstring = require("randomstring");

const secretkey = 'fc57e56a4eff07abd5a9929e5cd0941e'

const mysql = require('mysql')


/**
 * connect to mysql database hogrpg
 * @type {Connection}
 */
const connection = mysql.createConnection({
    host    : 'localhost',
    user    : 'root',
    password: 'sqldb',
    database: 'hogrpg'
});

connection.connect(err => {
    if(err){
        console.log(err)
    }
    else{
        console.log("Connected to mysql database")
    }
})

//end connection segment

/**
 * verify JWT-token send by user and sets res.userID to userID saved in token
 * @param req
 * @param res
 * @param next
 * @returns {*|void}
 */
function verifyToken(req,res,next){
    if(!req.headers.authorization){
        return res.status(401).send("Unauthorized request")
    }
    const token = req.headers.authorization.split(' ')[1]
    if(token === ''){
        return res.status(401).send("Unauthorized request")
    }
    try{
        let payload = jwt.verify(token, secretkey)
        if(!payload){
            return res.status(401).send("Unauthorized request")
        }
        req.userId = payload.subject
        next()
    }
    catch(err){
        res.status(401).send("Unauthorized request")
    }



}

/**
 * desperated
 */
router.post('/checkUsername',(req,res) =>{
    let userData = req.body
    let username =  req.body.username

    connection.query('SELECT * FROM users WHERE username ="'+username+'"',function(err,rows,fields){
        if(err){
            console.log(err)
            res.status("500"),send();
        }
        else{
            console.log(rows.length)
            if(rows.length > 0){
                res.status("200").send({'free':false})
            }else{
                res.status("200").send({'free':true})
            }
        }
    //TODO



    })

})

/**
 * POST request to register new user
 * token = username + rand()
 */
router.post('/register', (req,res) => {
    let username = req.body.username
    let email = req.body.email
    let password = req.body.password
   // let salt = req.body.salt

    connection.query('SELECT * FROM users WHERE username ="'+username+'"',function(err,rows,fields){
        if(err){
            console.log(err)
            res.status("500").send()
        }
        else{
            console.log(rows.length)
            if(rows.length === 0){
                connection.query('SELECT * FROM users WHERE email ="'+email+'"',function(err,rows,fields){
                    if(err){
                        console.log(err)
                        res.status("500").send()
                    }
                    else{
                        if(rows.length === 0){

                            let salt = randomstring.generate(42)
                            password = md5(password+salt)

                            const token = username+randomstring.generate(20);

                            connection.query('INSERT INTO verify(username,email,password,salt,token) VALUES ("' + username + '","' + email + '","' + password + '","'+salt+'","'+token+'")', function (err, rows, fields) {
                                if (err) {
                                    console.log(err)
                                    res.status(500).send()
                                } else {
                                    //VALID Registration
                                    //TODO sendEmail


                                        // create reusable transporter object using the default SMTP transport
                                        let transporter = mailer.createTransport({
                                            host: 'smtp.gmail.com',
                                            port: 465,
                                            secure: true,
                                            auth: {
                                                user: "hgdrpg@gmail.com",
                                                pass: "hgd18#dev_mf1"
                                            }
                                        });
                                        // setup email data with unicode symbols
                                        let mailOptions = {
                                            from: '"Hogwarts Diary👻" <hogrpg@gmail.com>', // sender address
                                            to: email, // list of receivers
                                            subject: 'Verify your account ✔', // Subject line
                                            text: 'Verify your account', // plain text body
                                            html: '<b>localhost:4200/verify/' + token + '</b>' // html body
                                        };

                                        transporter.sendMail(mailOptions, (error, info) => {
                                            if (error) {
                                                return console.log(error);
                                            }
                                            console.log('Message sent: %s', info.messageId);
                                            // Preview only available when sending through an Ethereal account
                                            console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

                                            // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
                                            // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
                                        });



                                    res.status(200).send({msg: 'send'});
                                }
                            })



                        }
                        else{
                            //email already used
                            res.status(401).send({'error':'email_is_used'})
                        }
                    }

                })

            }
            else{
                res.status(401).send({'error':'username_is_used'})
            }
        }




    })



})


/**
 * New Function!
 */
router.post('/verify/:token', (req,res) =>{
    const token = req.params.token;
    connection.query('SELECT * FROM verify WHERE token ="'+token+'"',function(err,rows,fields){
        console.log(rows);
        if(rows.length !== 0){
            console.log(rows);
            const id = rows[0].id;
            connection.query('INSERT INTO users(username,email,password,salt) VALUES ("' + rows[0].username + '","' + rows[0].email + '","' + rows[0].password + '","'+rows[0].salt+'")', function (err, rows, fields) {
                if (err) {
                    console.log(err)
                    res.status(500).send()
                } else {
                    //VALID Registration
                    connection.query('DELETE  FROM verify WHERE id ="'+id+'"',function(err,rows,fields){
                        if(err){
                            console.log(err);
                            res.status(500).send();
                        }
                        res.status(200).send();
                    })
                }
            })
        }else{
            res.status(401).send()
        }
    })


})
/**
 * {
 *   login_name : <username>||<email>,
 *   password : <pasword>
 * }
 */

router.post('/login',(req,res) => {
    let login_name = req.body.login_name
    let password = req.body.password

    console.log(login_name)
    console.log(password)
    //get salt
    connection.query('SELECT salt FROM users WHERE (username = "' + login_name + '" OR email = "' + login_name + '")', function (err, rows, fields) {
        if (err) {
            res.status(500).send()
        } else {

            if (rows.length > 0) {

                password = md5(password + rows[0].salt) //hashed password

                connection.query('SELECT id FROM users WHERE (username = "' + login_name + '" OR email = "' + login_name + '") AND password = "' + password + '"', function (err, rows, fields) {
                    if (err) {
                        console.log(err)
                        res.status("500")
                    } else {
                        console.log(password)
                        if (rows.length > 0) {

                            let payload = {subject: rows[0].id}
                            let token = jwt.sign(payload,secretkey)
                            console.log(token)
                            res.status(200).send({token}) //GET ID
                        } else {
                            res.status(401).send();
                            console.log("not found")
                        }

                    }
                })

            } else {
                res.status(401).send()
            }
        }
    })
})


router.post('/house',verifyToken, (req,res) =>{
    connection.query('SELECT * FROM users WHERE id ="'+req.userId+'"',function(err,rows,fields){
        if(err){
            res.status(500).send();
        } else{
            res.status(200).send({'house':rows[0].house});
        }
    })
})

router.post('/user',verifyToken,(req,res) =>{
    connection.query('SELECT * FROM users WHERE id ="'+req.userId+'"',function(err,rows,fields){
        if(err){
            res.status(500).send();
        }else{
            res.status(200).send({'name':rows[0].name,'surname':rows[0].surname,'age':rows[0].age,'house':rows[0].house,'job':rows[0].job});
        }
    })
})

router.post('/chats',verifyToken,(req,res) =>{
    connection.query("SELECT chatlist.cid, chats.name FROM chatlist INNER JOIN chats ON chatlist.cid = chats.id AND chatlist.uid = "+req.userId+";",function(err,rows,fields){
        if(err) {
            res.status(500).send();
        } else {
            res.status(200).send({'chats':rows})
        }
    })
})

router.post('/allChats',verifyToken, (req,res) =>{
    connection.query('SELECT * FROM chats',function(err,rows,fields){
       if(err){
           res.status(500).send();
       }else{
           res.status(200).send({'chats':rows});
       }
    })
})

router.post('/addChat',verifyToken, (req,res) =>{

    //check if already set
    connection.query('SELECT * FROM chatlist WHERE uid = '+req.userId+' AND cid = '+req.body.cid+';',function(err,rows,fields){
        if(err){
            console.log(err);
            res.status(500).send();
        }else{
            if(rows.length === 0){
                //TODO if

                connection.query('SELECT * FROM chats WHERE id = '+req.body.cid+' AND chats.key = "'+req.body.key+'";',function(err,rows,fields){
                    if(err){
                        res.status(500).send();
                        console.log(err);
                    }else{
                        if(rows.length !== 0){
                            connection.query('INSERT INTO chatlist(uid,cid) VALUES('+req.userId+','+req.body.cid+');',function(err,rows,fields){
                                if(err){
                                    console.log(err);
                                    res.status(500).send();
                                }else{
                                    res.status(200).send();
                                }
                            })
                        }
                        else{
                            res.status(401).send();
                        }

                    }
                })

            }else{
                res.status(409).send();
            }

        }
    })

})


router.post('/messages/:cid',verifyToken, (req,res) =>{
    connection.query("SELECT messages.message, messages.timestamp, users.username, CASE WHEN messages.uid = "+req.userId+" THEN 'true' ELSE 'false' END as self   FROM messages INNER JOIN users ON messages.uid = users.id AND cid = "+req.params.cid+";",function (err,rows,fields) {
        if(err) {
            res.status(500).send()
        } else {
            //JOIN and
            res.status(200).send({'messages':rows})
        }
    })
    //post id of last msg ->
    // get all new -> badge = anz(new)
})

router.post('/addMessage/:cid',verifyToken,(req,res) =>{
        msg = req.body.message;
        cid = req.params.cid;
        uid = req.userId;
        timestamp =  moment().format("YYYY-MM-DD hh:mm:ss.ms");
        console.log(msg)
        connection.query("INSERT INTO messages(cid,uid,message,timestamp) VALUES ("+cid+","+uid+",'"+msg+"','"+timestamp+"')",function(err,rows,fields){
        if(err) {
            console.log(err);
            res.status(500).send();
        } else {
            res.status(200).send();
        }
    })

})

module.exports = router