const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')

const PORT = 1234
const api = require('./routes/api')
const app = express()

//app.use(cors)
app.use(function(req,res,next){
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With,contentType,Content-Type, Accept, Authorization");
    next();
})
app.use(bodyParser.json())
app.use('/api',api)

app.get('/',function(req,res){
    res.send('Hello') //Basic Information about API
})

app.listen(PORT,function(){
    console.log('Server running on localhost:'+PORT)
})