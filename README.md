# HogwartsDiary

### Genreal:
*   language: Englisch

### Commits: [Conventional Style](https://www.conventionalcommits.org/en/v1.0.0-beta.2/)
### Branching Model:
*   Master: **only** contains working snapshots
*   hotfix: Fix bugs of Master
*   develop: branch to develop on
*   feature-branch: branches for different features
[Branching Convention](https://nvie.com/posts/a-successful-git-branching-model/)

### Git-Structure
*   Code: here goes all the Code
** 	Server: Backend
** 	Client: Angular-App
*   File Directory : here goes all files to support the projekt
 

### Coding-Convention
*  to be specified
